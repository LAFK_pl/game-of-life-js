# Game of Life

Thank you Mr Conway for hours of fun here. :-)

Simple JS code doing a simple game of life, on a board 25x25. 

## Features:

1. Random initialization (or reinitialization if your game is ongoing)
1. You may pause and add / remove cells and hit continue
1. You may clear the board if you want to
1. Speed can be adjusted - time per generation can be as low as 100ms or as high as 1s
1. Simple rules (Lazarus case, death of over/under-population)

Demo page: https://lafk_pl.gitlab.io/game-of-life-js/plansza.html

TBD: EN language version, larger board, configurable board (via UI), better code.
