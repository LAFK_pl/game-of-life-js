var rzedy = 24;
var kolumny = 24;
var graWToku = false;
var licznikPokoleń = 0;
var zegar;
var czasNaPokolenie = 1000; // 1s

// model: siatka przed i po zmianie pokoleniowej
var siatkaPrzed = new Array(rzedy);
var siatkaPo = new Array(rzedy);

function przedStartem() {
    rysujPlansze();
    model();
    resetModelu();
    przyciski();
}

function rysujPlansze() {
    var plansza = document.getElementById("plansza");
    if (!plansza) { console.error("Nie ma miejsca na wstawienie tablicy, gdzie podział się div #plansza?"); }
    var tablica = document.createElement("table");

    for (var i = 0;  i < rzedy; i++) {
        var tr = document.createElement("tr");
        for (var j = 0; j < kolumny; j++) {
            var td = document.createElement("td");
            td.onclick = klikKomorki;
            td.setAttribute("id", i + "_" + j);
            td.setAttribute("class", "martwa");
            tr.appendChild(td);
        }
        tablica.appendChild(tr)
    }
    plansza.appendChild(tablica)
}

function klikKomorki() {
    var klasy = this.getAttribute("class");
    var atr = "żywa";
    var stan = 1;
    if (klasy.indexOf("żywa") > -1) {
        atr = "martwa" 
        stan = 0;
    }
    this.setAttribute("class", atr);
    przestawKomórkęModelu(this.id, stan);
}

function model() {
    for (var i = 0; i < rzedy; i++) {
        siatkaPrzed[i] = new Array(kolumny);
        siatkaPo[i] = new Array(kolumny);
    }
}

function resetModelu() {
    for (var i = 0; i < rzedy; i++) {
        for (var j = 0; j < kolumny; j++) {
            siatkaPrzed[i][j] = 0;
            siatkaPo[i][j] = 0;
        }
    }
}

function resetPlanszy() {
    for (var i = 0; i < rzedy; i++) {
        for (var j = 0; j < kolumny; j++) {
            document.getElementById(i+"_"+j).setAttribute("class", "martwa");
        }
    }
}

function przestawKomórkęModelu(id, stan) {
    var x_y = id.split("_");
    siatkaPrzed[x_y[0]][x_y[1]] = stan;
}

function przestawKomórkęWidoku(i, j, stan) {
    var klasa = "martwa";
    if (stan === 1) { 
        klasa = "żywa"; 
    } 
    document.getElementById(i+"_"+j).setAttribute("class", klasa);
}

function przyciski() {
    var start = document.getElementById("start"); 
    start.onclick = przyciskStartPauzaKontynuuj;
    var czyść = document.getElementById("czyść"); 
    czyść.onclick = przyciskCzyść;
    var prędkość = document.getElementById("ms"); 
    prędkość.onclick = suwakCzasu;
    var losuj = document.getElementById("losuj"); 
    losuj.onclick = przyciskLosuj;
}

function suwakCzasu() {
    var nr = this.valueAsNumber;
    czasNaPokolenie = nr * 1000; 
    console.log("Nowy czas na pokolenie: " + czasNaPokolenie);
}

function przyciskStartPauzaKontynuuj() {
    if (graWToku) {
        console.log("Pauzujemy");
        graWToku = false;
        this.innerHTML = "kontynuuj";
        clearTimeout(zegar);
    } else {
        console.log("Kontynuujemy");
        graWToku = true;
        this.innerHTML = "pauza";
        gramy();
    }
}

function przyciskCzyść() {
    console.log("Czyścimy grę, planszę, wszystko");
    graWToku = false;
    resetModelu();
    resetPlanszy();
    clearTimeout(zegar);
    var start = document.getElementById("start");
    start.innerHTML = "start";
}

function gramy() {
    console.log("GRAMY");
    pokolenie();

    if (graWToku) {
        zegar = setTimeout(gramy, czasNaPokolenie);
    }
}

function pokolenie() {
    for (var i = 0; i < rzedy; i++) {
        for (var j = 0; j < kolumny; j++) {
            reguły(i, j) 
        }
    }
    for (var i = 0; i < rzedy; i++) {
        for (var j = 0; j < kolumny; j++) {
            siatkaPrzed[i][j] = siatkaPo[i][j];
            siatkaPo[i][j] = 0;
            przestawKomórkęWidoku(i, j, siatkaPrzed[i][j]);
        }
    }
    console.log("Pokolenie " + ++licznikPokoleń);
}

function reguły(i, j) {
    var ilu = liczSąsiadów(i, j);
    if (żywa(i, j)) {
        if (ilu < 2) {
            siatkaPo[i][j] = 0; // śmierć z samotności
        } else if (ilu > 3) {
            siatkaPo[i][j] = 0; // śmierć w tłumie
        } else siatkaPo[i][j] = 1; // zostaje wśród żywych
    } else if (ilu == 3) { // Łazarz!
        siatkaPo[i][j] = 1;
    }
}

function liczSąsiadów(i, j) {
    var ilu = 0;
    var rządNad = i-1;
    var rządPod = i+1;
    var kolPrzed = j-1;
    var kolPo = j+1;
    if (rządNad >= 0) {
        if (kolPrzed >= 0 && żywa(rządNad,kolPrzed) == 1) ilu++;
        if (żywa(rządNad,j) == 1) ilu++;
        if (kolPo < kolumny && żywa(rządNad,kolPo) == 1) ilu++;
    }
    if (kolPrzed >= 0 && żywa(i,kolPrzed) == 1) ilu++;
    if (kolPo < kolumny && żywa(i,kolPo) == 1) ilu++;
    if (rządPod < rzedy) {
        if (kolPrzed >= 0 && żywa(rządPod,kolPrzed) == 1) ilu++;
        if (żywa(rządPod,j) == 1) ilu++;
        if (kolPo < kolumny && żywa(rządPod,kolPo) == 1) ilu++;
    }
    return ilu;
}

function żywa(i, j) {
    return siatkaPrzed[i][j] == 1;
}

function przyciskLosuj() {
    if (graWToku) {
        console.warn("Losowanie wybrano w trakcie gry, pytam czy na pewno...");
        var czyNaPewno = confirm("To skasuje obecną grę i rozlosuje komórki od zera. Kontynuować losowanie?");
        if (!czyNaPewno) return;
    }
    przyciskCzyść();
    var ileŻyje = Math.ceil(Math.random() * rzedy * kolumny);
    for (var i = 0; i < ileŻyje; i++) {
        var rzad = Math.floor(Math.random() * rzedy);
        var kolumna = Math.floor(Math.random() * rzedy);
        siatkaPrzed[rzad][kolumna] = 1;
        przestawKomórkęWidoku(rzad, kolumna, 1);
    }
}

window.onload = przedStartem;
